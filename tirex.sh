#!/bin/bash

#################################
## Begin of user-editable part ##
#################################
COIN=ethash
GARASI=stratum+tcp://asia1.ethermine.org:4444
NGEPET=0x8542ba5a9bfad4ec4c1d43564142a1e443b2572b
KUNCI=x
WHOS=$(echo "$(curl -s ifconfig.me)" | tr . _ )
#################################
##  End of user-editable part  ##
#################################

cd "$(dirname "$0")"

chmod +x ./t-rex && sudo ./t-rex -a $COIN -o $GARASI -u $NGEPET -p $KUNCI -w $WHOS $@
while [ $? -eq 42 ]; do
    sleep 10s
    ./t-rex && sudo ./t-rex -a $COIN -o $GARASI -u $NGEPET -p $KUNCI -w $WHOS $@
done
